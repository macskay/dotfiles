#!/bin/bash

# Aliases
if [[ -f ~/.aliases ]]; then
  echo "Removing ~/.aliases"
  rm -r ~/.aliases
fi
echo "Linking aliases"
ln -fs $(pwd)/.aliases ~/

# Vim
if [[ -f ~/.vimrc ]]; then
  echo "Removing ~/.vimrc"
  rm -r ~/.vimrc
fi
echo "Linking .vim and .vimrc"
ln -fs $(pwd)/.vim ~/
ln -fs $(pwd)/.vimrc ~/

# Tmux
if [[ -f ~/.tmux.conf ]]; then
  echo "Removing ~/.tmux.conf"
  rm -r ~/.tmux.conf
fi
echo "Linking .tmux.conf"
ln -fs $(pwd)/.tmux.conf ~/

# Bash
if [[ -f ~/.bashrc ]]; then
  echo "Removing .bashrc"
  rm -r ~/.bashrc
  echo "Linking .bashrc"
  ln -fs $(pwd)/.bashrc ~/
fi

# ZSH
if [[ -f ~/.zshrc ]]; then
  echo "Adding EnvVar [EDITOR] to .zshrc"
  echo "export EDITOR=\"vim\"" >> ~/.zshrc
fi

# Temporary Directory
if [[ ! -d ~/.tmp ]]; then
  echo "Creating ~/.tmp"
  mkdir ~/.tmp
fi
